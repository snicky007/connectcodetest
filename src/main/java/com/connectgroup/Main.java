package com.connectgroup;

import java.io.FileNotFoundException;
import java.io.*;
import com.connectgroup.DataFilterer;

class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Reader sourceFile = DataFilterer.readCSVfile();
        DataFilterer.filterByCountry(sourceFile, "US");
    }
}
