package com.connectgroup;

import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.*;

public class DataFilterer {
    public static Reader targetFile;
    public static int data;
    public static ArrayList<String[]> parsedData;

    public static Collection<?> filterByCountry(Reader source, String country) throws IOException {

        ArrayList<String[]> parsedData = parseFileData(source);
        for (int i = 0; i < parsedData.size(); i++) {
            String[] eachLine = parsedData.get(i);
            if (eachLine[1].equals(country)) {
                System.out.println(String.join(",", eachLine));
                break;
            }

        }
        return Collections.emptyList();
    }

    public static Collection<?> filterByCountryWithResponseTimeAboveLimit(Reader source, String country, long limit) {
        return Collections.emptyList();
    }

    public static ArrayList<String[]> parseFileData(Reader source) throws IOException {
        StringBuilder str = new StringBuilder("");
        ArrayList<String[]> parsedData = new ArrayList<String[]>();
        String[] eachLine = new String[3];
        int i = 0;
        int data;
        boolean header = true;
        do {
            data = source.read();
            char dataChar = (char) data;
            if (dataChar == ',') {
                eachLine[i] = new String(str);
                i = i + 1;

                str = new StringBuilder("");
                continue;
            }
            if (dataChar == '\n') {
                eachLine[i] = new String(str);
                i = 0;
                str = new StringBuilder("");

                if (header == true) {
                    header = false;
                } else {
                    parsedData.add(eachLine);
                }

                eachLine = new String[3];
                continue;
            }
            str.append(dataChar);
        } while (data != -1);

        return parsedData;
    }

    public static Collection<?> filterByResponseTimeAboveAverage(Reader source) throws IOException {
        ArrayList<String[]> parsedData = parseFileData(source);
        for (int i = 0; i < parsedData.size(); i++) {
            String[] eachLine = parsedData.get(i);
            for (int j = 0; j < eachLine.length; j++) {

            }
        }
        return Collections.emptyList();
    }

    public static Reader readCSVfile() throws FileNotFoundException {
        targetFile = (Reader) new FileReader("test.csv");
        return targetFile;
    }
}